package com.mendoza.truenorthlm.models

class BaseResponse<T>(val data:T?, val responseCode:Int?, val errorDesc:String?) {
}