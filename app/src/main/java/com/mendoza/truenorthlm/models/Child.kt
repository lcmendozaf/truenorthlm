package com.mendoza.truenorthlm.models

import androidx.recyclerview.widget.DiffUtil

data class Child(
    val `data`: DataX?,
    val kind: String?,
    var isHidden:Int? = 0
) {
    object DIFF_CALLBACK : DiffUtil.ItemCallback<Child>() {
        override fun areItemsTheSame(oldItem: Child, newItem: Child): Boolean {
            return oldItem.data?.id == newItem.data?.id
        }

        override fun areContentsTheSame(oldItem: Child, newItem: Child): Boolean {
            return oldItem == newItem
        }

    }
}