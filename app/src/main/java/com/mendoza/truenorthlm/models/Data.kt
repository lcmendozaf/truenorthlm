package com.mendoza.truenorthlm.models

data class Data(
    val after: String?,
    val before: Any?,
    val children: List<Child>?,
    val dist: Int?,
    val geo_filter: String?,
    val modhash: String?
)