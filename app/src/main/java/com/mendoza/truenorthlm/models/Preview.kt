package com.mendoza.truenorthlm.models

data class Preview(
    val enabled: Boolean?,
    val images: List<Image>?
)