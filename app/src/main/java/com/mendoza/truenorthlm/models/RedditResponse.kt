package com.mendoza.truenorthlm.models

data class RedditResponse(
    val `data`: Data?,
    val kind: String?
)