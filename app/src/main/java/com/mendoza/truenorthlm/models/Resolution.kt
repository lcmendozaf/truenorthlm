package com.mendoza.truenorthlm.models

data class Resolution(
    val height: Int?,
    val url: String?,
    val width: Int?
)