package com.mendoza.truenorthlm.models

data class SecureMedia(
    val reddit_video: RedditVideoX?
)