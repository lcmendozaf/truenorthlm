package com.mendoza.truenorthlm.models

data class Source(
    val height: Int?,
    val url: String?,
    val width: Int?
)