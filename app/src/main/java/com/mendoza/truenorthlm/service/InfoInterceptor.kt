package com.mendoza.truenorthlm.service

import com.mendoza.truenorthlm.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class InfoInterceptor:Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .addHeader("User-Agent", "android:"+BuildConfig.APPLICATION_ID+":"+BuildConfig.VERSION_NAME)
            .addHeader("Content-Type","application/json")
            .build()
        return chain.proceed(request)
    }
}