package com.mendoza.truenorthlm.service

import com.mendoza.truenorthlm.models.EmptyResponse
import com.mendoza.truenorthlm.models.RedditResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface RedditApi {
    @GET("/top/.json")
    suspend fun getTopReddit(@Query("limit") limit:Int, @Query("after") nextList:String?): RedditResponse

    @GET("/top/.json")
    fun getTopRedditPaging(@Query("limit") limit:Int, @Query("after") nextList:String?): Call<RedditResponse>

    @POST("/api/read_message")
    fun marAsRead(name:String):Call<EmptyResponse>
}