package com.mendoza.truenorthlm.service

import com.mendoza.truenorthlm.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor

class RedditClient {

    companion object {
        private val BASE_URL = "https://www.reddit.com"
        private var retrofit: Retrofit? = null

        fun getRetrofit():Retrofit {
            return retrofit ?:
                Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(buildRetrofitClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }

        private fun buildRetrofitClient():OkHttpClient {
            val builder = OkHttpClient.Builder()
            if(BuildConfig.DEBUG) {
                val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
                    this.level = HttpLoggingInterceptor.Level.BODY
                }
                builder.addInterceptor(httpLoggingInterceptor)
            }
            builder.addInterceptor(InfoInterceptor())
            return builder.build()
        }
    }
}