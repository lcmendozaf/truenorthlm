package com.mendoza.truenorthlm.service.repositories

import android.content.Context
import com.mendoza.truenorthlm.models.BaseResponse
import com.mendoza.truenorthlm.models.RedditResponse
import com.mendoza.truenorthlm.service.RedditApi
import com.mendoza.truenorthlm.service.RedditClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostsRepository {

    companion object {
        private var INSTANCE : PostsRepository? = null

        fun getInstance() = INSTANCE ?: PostsRepository().also { INSTANCE = it }

        val FETCH_LIMIT = 20
    }

    fun getPosts(limit:Int = FETCH_LIMIT, after:String?, onResult: (BaseResponse<RedditResponse>) -> Unit) {
        RedditClient.getRetrofit().create(RedditApi::class.java)
            .getTopRedditPaging(limit, after).enqueue(object : Callback<RedditResponse> {
                override fun onResponse(
                    call: Call<RedditResponse>,
                    response: Response<RedditResponse>
                ) {
                    if(response.isSuccessful) {
                        onResult(BaseResponse(response.body(), response.code(), null))
                    } else
                        onResult(BaseResponse(null, response.code(), response.errorBody()?.string()))
                }

                override fun onFailure(call: Call<RedditResponse>, t: Throwable) {
                    onResult(BaseResponse(null, null, null))
                }
            })
    }
}