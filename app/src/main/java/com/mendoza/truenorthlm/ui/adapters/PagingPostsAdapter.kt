package com.mendoza.truenorthlm.ui.adapters

import android.animation.Animator
import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import androidx.core.text.HtmlCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mendoza.truenorthlm.R
import com.mendoza.truenorthlm.databinding.PostAdapterLayoutBinding
import com.mendoza.truenorthlm.models.Child
import com.mendoza.truenorthlm.utils.getCreatedTimeString
import com.squareup.picasso.Picasso

class PagingPostsAdapter(val onPostClicked:(Child) -> Unit): PagingDataAdapter<Child, PagingPostsAdapter.ViewHolder>(Child.DIFF_CALLBACK) {
//    private var postsList: List<Children>? = null
    private var itemsRemoved = ArrayList<String>()

    enum class VisibilityStates(val state:Int) {
        Visible(0),
        Hiding(1),
        Hidden(2)
    }

    companion object {
        val DismissDuration = 300L
    }

    inner class ViewHolder(val binding:PostAdapterLayoutBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(PostAdapterLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val postId = getItem(position)?.data?.id ?: ""
        holder.binding.root.tag = postId
        with(getItem(position)?.data) {
            holder.binding.title.text = HtmlCompat.fromHtml(this?.title ?: "", HtmlCompat.FROM_HTML_MODE_COMPACT)
            if(this?.thumbnail.isNullOrEmpty())
                holder.binding.image.visibility = View.GONE
            else {
                Log.d(PagingPostsAdapter::class.java.name, "Image URL "+this?.thumbnail)
                holder.binding.image.visibility = View.VISIBLE
                val imageRes :Int
                if(this?.thumbnail?.length!! < 4)
                    imageRes = R.drawable.ic_image_not_supported_24
                else {
                    val endingString = this.thumbnail.subSequence((this.thumbnail.length - 4).rangeTo(this.thumbnail.length - 1))
                    Log.d(PagingPostsAdapter::class.java.name, " - end"+endingString)
                    when (endingString) {
                        "nsfw" -> imageRes = R.mipmap.ic_nsfw
                        ".jpg" -> {
                            imageRes = -1
                            holder.binding.image.setOnClickListener { onPostClicked(getItem(position)!!) }
                        }
                        else -> imageRes = R.drawable.ic_image_not_supported_24
                    }
                }
                Picasso.get()
                    .load(this.thumbnail)
                    .placeholder(R.drawable.progress_animation)
                    .error(imageRes)
                    .config(Bitmap.Config.ARGB_8888)
                    .into(holder.binding.image)
            }
            holder.binding.creationTime.text = getCreatedTimeString(this?.created_utc ?: -1.0)

            holder.binding.author.text = "by ${this?.author}"
            holder.binding.comments.text = "${this?.num_comments} comments"

            holder.binding.dismiss.setOnClickListener {
                removeViewHolder(holder)
            }
        }
        if((getItem(position)?.isHidden ?: VisibilityStates.Visible.state) == VisibilityStates.Hidden.state) {
            holder.binding.cardView.visibility = View.GONE
            holder.binding.root.setPadding(0,0,0,0)
        }
//        else if((getItem(position)?.isHidden ?: VisibilityStates.Visible.state) == VisibilityStates.Visible.state && itemsRemoved.contains(postId)) {
//            removeViewHolder(holder)
//        }
    }

    fun removeViewHolder(holder:ViewHolder) {
        val postId = holder.binding.root.tag as String
        itemsRemoved.add(postId)
        ObjectAnimator.ofFloat(holder.binding.cardView, "translationX", 0F, holder.binding.root.width.toFloat()).apply {
            duration = DismissDuration
            interpolator = AccelerateInterpolator(0.5F)
            start()
            getItemById(postId)?.isHidden = VisibilityStates.Hiding.state
            addListener(object:Animator.AnimatorListener {
                override fun onAnimationStart(p0: Animator?) {

                }

                override fun onAnimationEnd(p0: Animator?) {
                    getItemById(postId)?.isHidden = VisibilityStates.Hidden.state
                    notifyDataSetChanged()
                }

                override fun onAnimationCancel(p0: Animator?) {

                }

                override fun onAnimationRepeat(p0: Animator?) {

                }
            })
        }
    }

    private fun getItemById(id:String) : Child? {
        for(i in 0 until itemCount) {
            if(getItem(i)?.data?.id == id)
                return getItem(i)
        }
        return null
    }

    fun getPost(position:Int):Child? {
        return super.getItem(position)
    }

    fun clearItemsRemoved() {
        itemsRemoved.clear()
    }
}