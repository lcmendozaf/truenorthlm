package com.mendoza.truenorthlm.ui.adapters

import android.animation.Animator
import android.animation.ObjectAnimator
import android.graphics.Bitmap
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.mendoza.truenorthlm.R
import com.mendoza.truenorthlm.TrueNorthLM.Companion.appContext
import com.mendoza.truenorthlm.databinding.PostAdapterLayoutBinding
import com.mendoza.truenorthlm.models.Child
import com.mendoza.truenorthlm.utils.getCreatedTimeString
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow

class PostsAdapter(val onPostCallback:onPostClickCallback): RecyclerView.Adapter<PostsAdapter.ViewHolder>() {
    private var postsList = ArrayList<Child>()
    private var itemsRemoved = ArrayList<String>()

    enum class VisibilityStates(val state: Int) {
        Visible(0),
        Hiding(1),
        Hidden(2)
    }

    companion object {
        val DismissDuration = 500L
    }

    interface onPostClickCallback {
        fun onPostClicked(child:Child)
        fun onPostLongClick(child:Child)
        fun onPostDismissed()
    }

    inner class ViewHolder(val binding: PostAdapterLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(child: Child) {

            val postId = child.data?.id ?: ""
            binding.root.tag = postId
            with(child.data) {
                binding.title.text =
                    HtmlCompat.fromHtml(this?.title ?: "", HtmlCompat.FROM_HTML_MODE_COMPACT)
                if (this?.thumbnail.isNullOrEmpty())
                    binding.image.visibility = View.GONE
                else {
                    Log.d(PagingPostsAdapter::class.java.name, "Image URL " + this?.thumbnail)
                    binding.image.visibility = View.VISIBLE
                    val imageErrorRes: Int
                    if (this?.thumbnail?.length!! < 4)
                        imageErrorRes = R.drawable.ic_image_not_supported_24
                    else {
                        imageErrorRes = getImageResValue(this.thumbnail)
                    }
                    binding.image.setOnClickListener {
                        val item = postsList.firstOrNull { it.data?.id == postId }
                        item?.let { item ->
                            item?.data?.read = true
                            notifyDataSetChanged()
                            onPostCallback.onPostClicked(item!!)
                        }
                    }
                    binding.image.setOnLongClickListener {
                        val item = postsList.firstOrNull { it.data?.id == postId }
                        item?.let { item ->
                            onPostCallback.onPostLongClick(item)
                        }
                        return@setOnLongClickListener true
                    }
                    if (imageErrorRes == -1 && this.thumbnail.length <= 4) {
                        binding.image.visibility = View.GONE
                    } else
                        binding.image.visibility = View.VISIBLE

                    Picasso.get()
                        .load(this.thumbnail)
                        .placeholder(R.drawable.progress_animation)
                        .error(imageErrorRes)
                        .config(Bitmap.Config.ARGB_8888)
                        .into(binding.image)
                }
                binding.creationTime.text = getCreatedTimeString(this?.created_utc ?: -1.0)

                binding.author.text = appContext.getString(R.string.post_by_author,this?.author)
                binding.comments.text = appContext.getString(R.string.number_of_comments,this?.num_comments)

                binding.dismiss.setOnClickListener {
//                    val item = postsList.firstOrNull { it.data?.id == postId }
//                    val pos = postsList.indexOf(item)
                    removeViewHolder(this@ViewHolder)
//                    notifyItemRemoved(pos)
                    onPostCallback.onPostDismissed()
                }

                if (this?.read == true)
                    binding.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.read_post
                        )
                    )
                else
                    binding.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.white
                        )
                    )
            }
        }
    }

    fun removeViewHolder(holder: PostsAdapter.ViewHolder) {
        val postId = holder.binding.root.tag as String
        val item = getItemById(postId)
        val position = postsList.indexOf(item)

        val anim = AnimationUtils.loadAnimation(holder.binding.root.context, R.anim.slide_out_right)
        anim.setDuration(DismissDuration)
        holder.binding.root.startAnimation(anim)
        GlobalScope.launch {
            delay((DismissDuration*0.7).toLong())
            withContext(Dispatchers.Main) {
                if (position >= 0 && position < postsList.size) {
                    postsList.removeAt(position)
                    notifyItemRemoved(position)
                }
            }
        }
        return
    }

    private fun getItemById(id:String) : Child? {
        for(i in 0 until itemCount) {
            if(postsList[i]?.data?.id == id)
                return postsList[i]
        }
        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            PostAdapterLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postsList[position])
    }

    private fun getImageResValue(thumbnail: String): Int {
        try {
            val endingString =
                thumbnail.subSequence(0.rangeTo(4))
            Log.d(PagingPostsAdapter::class.java.name, " - end" + endingString)
            return when (endingString) {
                "nsfw" -> R.mipmap.ic_nsfw
                "http" -> -1
                else -> R.drawable.ic_image_not_supported_24
            }
        } catch (error:Exception) {
            return R.drawable.ic_image_not_supported_24
        }
    }

    fun appendData(data: List<Child>) {
        postsList.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = postsList.size

    fun clear() {
        postsList.clear()
        notifyDataSetChanged()
    }

    fun removeAllItems(onFinishedRemoving: () -> Unit) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                getItemsFlow().collectLatest { position ->
                    withContext(Dispatchers.Main) {
                        postsList.removeAt(position)
                        notifyItemRemoved(position)
                        if(postsList.isEmpty())
                            onFinishedRemoving()
                    }
                }
            }
        }
    }

    //Creates a flow to delete the first post until all are gone
    private fun getItemsFlow(): Flow<Int> = flow {
        for (i in 0 until itemCount) {
            delay(DismissDuration)
            emit(0) // emit next value
        }
    }
}