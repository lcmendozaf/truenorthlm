package com.mendoza.truenorthlm.ui.fragments

import android.animation.ObjectAnimator
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mendoza.truenorthlm.R
import com.mendoza.truenorthlm.databinding.FragmentRedditPostsBinding
import com.mendoza.truenorthlm.models.Child
import com.mendoza.truenorthlm.ui.adapters.PagingPostsAdapter
import com.mendoza.truenorthlm.ui.adapters.PostsAdapter
import com.mendoza.truenorthlm.ui.viewModels.PostsViewModel
import com.mendoza.truenorthlm.utils.CustomPager
import com.mendoza.truenorthlm.utils.launchDownloadManager
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

/**
 * A simple [Fragment] subclass.
 * Use the [RedditPostsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RedditPostsFragment : Fragment(), PostsAdapter.onPostClickCallback {
    private lateinit var binding:FragmentRedditPostsBinding

    private val viewModel:PostsViewModel by activityViewModels()

    private lateinit var customPager:CustomPager
    private var errorDialog:AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRedditPostsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.postsList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.postsList.adapter = PostsAdapter(this)

        binding.swipeRefresh.setOnRefreshListener(this::onSwipeRefresh)

        viewModel.networkState.observe(viewLifecycleOwner) {
            binding.swipeRefresh.isRefreshing = false
            when (it) {
                "empty" -> {
                    showEmptyLayout()
                }
                "fetched" -> {
                    binding.postsList.visibility = View.VISIBLE
                }
                "Error" -> {
                    //If there was data, don't show the emptyLayout
                    if(binding.postsList.adapter?.itemCount == 0)
                        showEmptyLayout()
                    context?.let {
                        if(errorDialog == null || !errorDialog!!.isShowing) {
                            errorDialog = AlertDialog.Builder(it)
                                .setMessage(R.string.an_error_ocurred)
                                .setPositiveButton(android.R.string.ok, null)
                                .create()
                            errorDialog?.show()
                        }
                    }
                }
            }
        }

        customPager = CustomPager(requireContext(), viewModel, binding.postsList)

        binding.deleteAll.setOnClickListener {
            if(binding.postsList.adapter?.itemCount ?: 0 > 0) {
                customPager.isPagingEnabled = false
                setViewsEnabled(false)
                binding.postsList.smoothScrollToPosition(0)
                (binding.postsList.adapter as PostsAdapter).removeAllItems {
                    showEmptyLayout()
                    setViewsEnabled(true)
                }
            }
        }
        if(savedInstanceState == null) {
            onSwipeRefresh()
        } else {
            binding.postsList.layoutManager?.onRestoreInstanceState(savedInstanceState?.getParcelable("recycler"))
        }
    }

    fun onSwipeRefresh() {
        binding.swipeRefresh.isRefreshing = true
        customPager.isPagingEnabled = true
        binding.emptyText.visibility = View.GONE
        setViewsEnabled(true)
        customPager.getFirstData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val listState = binding.postsList.layoutManager?.onSaveInstanceState()
        if(outState != null) {
            outState.putParcelable("recycler", listState)
        }
    }

    override fun onPostClicked(children: Child) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(children.data?.url))
            startActivity(intent)
        } catch (exception:Exception) {

        }
    }

    override fun onPostLongClick(child: Child) {
        child.data?.url?.let { url ->
            context?.let {
                launchDownloadManager(it, url)
                Toast.makeText(it, R.string.downloading_image, Toast.LENGTH_SHORT).show()
            }
        }
    }

    //Unlikely to happen due to the paging...
    override fun onPostDismissed() {
        if(binding.postsList.adapter?.itemCount == 0) {
            showEmptyLayout()
        }
    }

    //Disables all views to prevent human interaction while removing posts
    fun setViewsEnabled(enabled:Boolean) {
        binding.swipeRefresh.isEnabled = enabled
        binding.deleteAll.isEnabled = enabled
        if(!enabled) {
            binding.postsList.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
                return@OnTouchListener true
            })
        } else {
            binding.postsList.setOnTouchListener(null)
        }
    }

    fun showEmptyLayout() {
        binding.postsList.visibility = View.GONE
        binding.emptyText.visibility = View.VISIBLE
        ObjectAnimator.ofFloat(binding.emptyText, View.ALPHA, 0F, 1F).apply {
            duration = 600
            start()
        }
    }
}
