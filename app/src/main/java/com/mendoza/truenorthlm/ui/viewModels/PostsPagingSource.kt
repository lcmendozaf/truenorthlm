package com.mendoza.truenorthlm.ui.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.mendoza.truenorthlm.models.Child
import com.mendoza.truenorthlm.service.RedditApi
import retrofit2.HttpException
import java.io.IOException

class PostsPagingSource(private val redditService: RedditApi, val networkState : MutableLiveData<String>): PagingSource<String, Child>() {
    override suspend fun load(params: LoadParams<String>):
            LoadResult<String, Child> {
        return try {
            val response = redditService.getTopReddit(FETCH_LIMIT, params.key)

            val listing = response.data
            val redditPosts = listing?.children
            networkState.postValue(if(redditPosts.isNullOrEmpty()) "empty" else "fetched" )
            return LoadResult.Page(
                redditPosts ?: listOf(),
                listing?.before,
                listing?.after
            ) as LoadResult<String, Child>
        } catch (exception: IOException) {
            networkState.postValue("Error")
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            networkState.postValue("Error")
            return LoadResult.Error(exception)
        }
    }

    companion object {
        val FETCH_LIMIT = 20
    }

    override fun getRefreshKey(state: PagingState<String, Child>): String? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.nextKey
        }
    }
}