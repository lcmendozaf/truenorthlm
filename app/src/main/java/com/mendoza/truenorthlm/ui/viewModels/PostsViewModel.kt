package com.mendoza.truenorthlm.ui.viewModels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.mendoza.truenorthlm.models.BaseResponse
import com.mendoza.truenorthlm.models.Child
import com.mendoza.truenorthlm.models.RedditResponse
import com.mendoza.truenorthlm.service.RedditApi
import com.mendoza.truenorthlm.service.RedditClient
import com.mendoza.truenorthlm.service.repositories.PostsRepository
import com.mendoza.truenorthlm.service.repositories.PostsRepository.Companion.FETCH_LIMIT
import kotlinx.coroutines.flow.Flow

class PostsViewModel:ViewModel() {

    val networkState = MutableLiveData<String>()
    val newDataLiveData = MutableLiveData<BaseResponse<RedditResponse>>()
    var loading = false

    fun fetchPosts(context: Context) : Flow<PagingData<Child>> {
        val newResult:Flow<PagingData<Child>> = getRepositoryStream(context)
        return newResult
    }

    fun getRepositoryStream(context: Context):Flow<PagingData<Child>> {
        return Pager(
            config = PagingConfig(PostsPagingSource.FETCH_LIMIT,
            enablePlaceholders = true),
            pagingSourceFactory = { PostsPagingSource(RedditClient.getRetrofit().create(RedditApi::class.java), networkState) }
        ).flow
    }

    fun getTopPosts(next:String?) {
        PostsRepository.getInstance().getPosts(FETCH_LIMIT, next) {
            newDataLiveData.postValue(it)
        }
    }

    fun getNexData() {
        PostsRepository.getInstance().getPosts(FETCH_LIMIT, newDataLiveData.value?.data?.data?.after) {
            newDataLiveData.postValue(it)
        }
    }
}