package com.mendoza.truenorthlm.utils

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mendoza.truenorthlm.ui.adapters.PostsAdapter
import com.mendoza.truenorthlm.ui.viewModels.PostsViewModel

class CustomPager(val context: Context, val viewModel: PostsViewModel, val recyclerView: RecyclerView) {

    private val visibleThreshold = 3

    var isPagingEnabled = true

    init {
        recyclerView.addOnScrollListener(object:RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = linearLayoutManager.getItemCount()
                val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (isPagingEnabled && !viewModel.loading && totalItemCount <= (lastVisibleItem + visibleThreshold + dy/5)) {
                    viewModel.getNexData()
                    viewModel.loading = true
                }
            }
        })

        viewModel.newDataLiveData.observe(context as AppCompatActivity) {
            viewModel.loading = false
            if(it.responseCode != 200) {
                viewModel.networkState.postValue("Error")
            } else {
                if(!it?.data?.data?.children.isNullOrEmpty()) {
                    (recyclerView.adapter as PostsAdapter).appendData(it?.data?.data?.children!!)
                    viewModel.networkState.postValue("fetched")
                } else
                    viewModel.networkState.postValue("empty")
            }
        }
    }

    fun getFirstData() {
        (recyclerView.adapter as PostsAdapter).clear()
        viewModel.getTopPosts(null)
    }
}