package com.mendoza.truenorthlm.utils

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import java.lang.Exception

fun launchDownloadManager(context:Context, url:String) {
    try {
        val urlUri = Uri.parse(url)
        val request = DownloadManager.Request(urlUri)
        request.setDescription("Downloading, please wait…")
        request.setTitle(urlUri.getLastPathSegment())
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//        request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        }
//    request.setVisibleInDownloadsUi(true)
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            urlUri.getLastPathSegment()
        )

        // get download service and enqueue file
        val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        manager.enqueue(request)
    } catch (error:Exception) {

    }
}