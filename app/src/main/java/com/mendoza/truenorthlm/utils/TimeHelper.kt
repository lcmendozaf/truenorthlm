package com.mendoza.truenorthlm.utils

import com.mendoza.truenorthlm.R
import com.mendoza.truenorthlm.TrueNorthLM.Companion.appContext
import org.joda.time.*
import java.util.*

fun getCreatedTimeString(createdTime:Double):String {
    if(createdTime == -1.0)
        return ""
    val today = DateTime()
    val creationTime = DateTime(createdTime.toLong() * 1000)
    val yearsPassed = Years.yearsBetween(creationTime, today).years
    if(yearsPassed > 0)
        return appContext.getString(R.string.years_ago,yearsPassed)
    val monthsPassed = Months.monthsBetween(creationTime, today).months
    if(monthsPassed > 0)
        return appContext.getString(R.string.months_ago,monthsPassed)
    val weeksPassed = Weeks.weeksBetween(creationTime, today).weeks
    if(weeksPassed > 0)
        return appContext.getString(R.string.weeks_ago,weeksPassed)
    val daysPassed = Days.daysBetween(creationTime, today).days
    if(daysPassed > 0)
        return appContext.getString(R.string.days_ago,daysPassed)
    val hoursPassed = Hours.hoursBetween(creationTime, today).hours
    if(hoursPassed > 0)
        return appContext.getString(R.string.hours_ago,hoursPassed)
    val minutesPassed = Minutes.minutesBetween(creationTime, today).minutes
    if(minutesPassed > 0)
        return appContext.getString(R.string.minutes_ago,minutesPassed)
    val secondsPassed = Seconds.secondsBetween(creationTime, today).seconds
    if(secondsPassed > 0)
        return appContext.getString(R.string.seconds_ago,secondsPassed)

    return creationTime.toString()
}